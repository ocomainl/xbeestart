//Group Z9 BRONZE challenge code
char userInput;
const int LEYE= A4; 
const int REYE= A3; 
int voltageL=0; 
int voltageR=0; 
const int threshold=400; 
const int TRIG = 9;
const int ECHO = 8;
int count =0;
const int encoder = 2;
volatile int counter = 0;
int LNEG = 4;
int LPOS = 5;
int RPOS = 6;
int RNEG = 7;

//Xbee 
//char userInput;
//boolean newData = false;
  
void setup() {
  pinMode(LNEG, OUTPUT);
  pinMode(LPOS, OUTPUT);
  pinMode(RPOS, OUTPUT);
  pinMode(RNEG, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(TRIG, OUTPUT);
  pinMode(encoder, INPUT);
  Serial.begin(9600);
  pinMode( LEYE , INPUT); 
  pinMode( REYE , INPUT); 
  attachInterrupt( digitalPinToInterrupt(encoder), ir_isr, RISING);
}

void ir_isr(){
    
  counter = counter +1;
}

void us(int& dis, int dur){
    
 digitalWrite(TRIG,LOW);
 delayMicroseconds(2);
 digitalWrite(TRIG, HIGH);
 delayMicroseconds(10);
 digitalWrite(TRIG,LOW);
 dur = pulseIn(ECHO, HIGH);
 dis = dur/58;
 
}

void loop() {
    
  voltageL = analogRead( LEYE); 
  voltageR = analogRead( REYE); 

  
if (Serial.available()) 
{ 
   userInput = Serial.read(); 
}
 
 if (userInput == 'g') {
 if ( voltageL > threshold ) { 
     
    digitalWrite(LNEG, HIGH);
    analogWrite(LPOS, 140);  
  
 }
 
 else { 
     
    digitalWrite(LNEG, HIGH);
    analogWrite(LPOS, 230);
    analogWrite(RPOS, 70);
    digitalWrite(RNEG, HIGH);
   
 }  
 
 if (voltageL < threshold && voltageR < threshold){ 
  
    analogWrite(RPOS, 150);
    digitalWrite(RNEG, HIGH);
     
    digitalWrite(LNEG, HIGH);
    analogWrite(LPOS, 210);
 
 }  
 
 if ( voltageR > threshold ) { 
     
    analogWrite(RPOS, 105);
    digitalWrite(RNEG, HIGH);
  
 }
 
 else { 
     
   analogWrite(RPOS, 210);
   digitalWrite(RNEG, HIGH);
   digitalWrite(LNEG, HIGH);
   analogWrite(LPOS, 110);  
  
 }
 
 //Ultrasonic sensor//
 int distance;
 int duration;
 
 if(count%10 ==0){
     
    us(distance, duration);
    Serial.println(distance);
    
    while((distance <= 20) && (distance >=0)){
        
        digitalWrite(LNEG, LOW);
        analogWrite(LPOS, 0);  
        analogWrite(RPOS, 0);
        digitalWrite(RNEG, LOW);
        delay(100);
        us(distance, duration);
        //Serial.println(distance);
        
        }
    }
 }
 else 
 {
  
     digitalWrite(LNEG, LOW);
     analogWrite(LPOS, 0);  
     analogWrite(RPOS, 0);
     digitalWrite(RNEG, LOW);
     delay(100);
  
 }
    //Serial.println("Count: ");
    //Serial.print(count);
 
 
 
 count ++;
  
 delay(30); 
}
